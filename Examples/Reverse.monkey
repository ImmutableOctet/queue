Strict

Public

' Imports:
Import queue
Import example

' Functions:
Function Main:Int()
	' Local variables:
	
	' Our queue of strings/lines.
	Local Lines:= New Queue<String>()
	Local LineStack:Stack<String>
	Local LineList:List<String>
	
	' Reverse containers:
	Local RLineStack:Stack<String> = Null
	Local RLines:Queue<String> = Null
	
	Print("Assigning elements...")
	AddMessageToQueue(Lines, ["You know the rules, and so do I."], True)
	
	Print("Reversing elements...")
	RLines = Lines.Reverse()
	
	Print("Building stack(s)...")
	LineStack = Lines.ToStack()
	RLineStack = RLines.ToStack()
	
	Print("Building list(s)...")
	LineList = Lines.ToList()
	
	Print(" ")
	Print("Process complete, displaying:")
	Print(" ")
	Print("Lines:")
	Print(" ")
	PrintQueue(Lines)
	
	Print(" ")
	Print("Lines in reverse:")
	Print(" ")
	PrintQueue(RLines, "Reversed")
	
	Print(" ")
	Print("How a stack would hold the same information (If added in reverse order):")
	Print(" ")
	PrintStack(RLineStack, "Reversed-stack-version")
	
	Print(" ")
	Print("How a stack would hold the same information (If added the same way as a queue):")
	Print(" ")
	PrintStack(LineStack, "Stack-version")
	
	Print(" ")
	Print("How a list would hold the same information (Done using 'AddLast'):")
	PrintList(LineList, "List-version")
	Print(" ")

	' Return the default response.
	Return 0
End

Function AddMessageToQueue:Void(Q:Queue<String>, Messages:String[]=[], ListChars:Bool=False)
	Local MessagesAdded:Bool = True

	If (Messages.Length() = 0) Then
		Messages = ["Hello world."]
		MessagesAdded = False
	Endif
	
	For Local S:String = Eachin Messages
		Q.Push(S)
	Next
	
	If (Not MessagesAdded Or MessagesAdded And ListChars) Then
		Local S:String
		
		For Local Index:Int = 0 Until Messages.Length()
			S = Messages[Index]
			
			For Local Chr:Int = Eachin S
				Q.Push(String.FromChar(Chr))
			Next
			
			If (Index <> Messages.Length()-1) Then
				Q.Push(" ")
			Endif
		Next
	Endif
	
	Return
End

' Classes/Other:
' Nothing so far.