#Rem
	This program is effectively a comparison example.
	
	Normally there are very basic comparisons done by a queue,
	these use standard means of checking equality.
	
	By having a class implement the 'ComparableInQueue' interface,
	you are able to write a deeper comparison function yourself.
#End

Strict

Public

' Imports:
Import queue
'Import retrostrings

' Global variables:
Global ErrorMessage:String = "A fatal-error has occurred; unable to continue."

' Functions:
Function Main:Int()
	Print("The 'Compare' demo:")
	Print(" ")
	Print("Creating queues...")
	
	' Local variables:
	
	' Our first queue (Used for comparisons later).
	Local FirstQueue:= New Queue<Test>()
	FirstQueue.Add(New Test(10, 20.0, "First entry", True))
	FirstQueue.Add(New Test(100, 200.0, "Second entry", False))
	
	' A completely different queue.
	Local SecondQueue:= New Queue<Test>()
	SecondQueue.Add(New Test(10, 20.0, "Second queue's first entry", True))
	SecondQueue.Add(New Test(22, 333.0, "Second queue's second entry", False))
	
	' A auto-generated clone of the first queue.
	Local ThirdQueue:= FirstQueue.Clone()
	
	' Effectively the same as the first queue.
	Local FourthQueue:= New Queue<Test>()
	FourthQueue.Add(New Test(10, 20.0, "First entry", True))
	FourthQueue.Add(New Test(100, 200.0, "Second entry", False))
	
	
	' Check for errors in the queues:
	'#Rem
	If (ErrorFound([FirstQueue, SecondQueue, ThirdQueue, FourthQueue])) Then
		#If CONFIG = "debug"
			Print(" ")
			Print("[ " + ErrorMessage + " ]")
			Print(" ")
		#Else
			Error(ErrorMessage)
		#End
	
		Return 1
	Endif
	'#End
	
	' Now that we have the queues created and setup, output to the console:
	Print("Comparing our newly created queues...")
	
	Print(" ")
	Print("Finished, result: " + BoolToString(Queue<Test>.CompareQueues(FirstQueue, SecondQueue)) + " - (FirstQueue & SecondQueue)")
	Print(" ")
	Print("Second result: " + BoolToString(Queue<Test>.CompareQueues(FirstQueue, FirstQueue)) + " - (FirstQueue & FirstQueue)")
	Print(" ")
	Print("Third result: " + BoolToString(Queue<Test>.CompareQueues(SecondQueue, SecondQueue)) + " - (SecondQueue & SecondQueue)")
	Print(" ")
	Print("Fourth result: " + BoolToString(Queue<Test>.CompareQueues(FirstQueue, ThirdQueue)) + " - (FirstQueue & ThirdQueue)")
	Print(" ")
	Print("Fifth result: " + BoolToString(Queue<Test>.CompareQueues(FirstQueue, FourthQueue)) + " - (FirstQueue & FourthQueue)")	
	Print(" ")
	
	Print("The 'Compare' demo has ended.")

	' Return the default response for the program.
	Return 0
End

Function BoolToString:String(Data:Bool)
	If (Data) Then Return "True"
	
	Return "False"
End

Function ErrorFound:Bool(Data:Queue<Test>[])
	Local Response:Bool = False

	For Local Q:= Eachin Data
		If (Q = Null) Then
			Response = True
		Else
			If (Q.Length() = 0) Then
				Response = True
			Else
				' Nothing so far.
			Endif
		Endif
		
		If (Response) Then Exit
	Next

	Return Response
End

' Classes:
' This interface is not required, but you can't do any extra comparison without it.
Class Test Implements ComparableInQueue
	' Constructor(s):
	Method New(A:Int=0, B:Float=0.0, C:String="", D:Object=Null)
		Self._New(A, B, C, D)
	End
	
	Method New(A:Int, B:Float, C:String, D:Bool)
		Self._New(A, B, C, New Other(D))
	End
	
	Method New(T:Test)
		If (T <> Null) Then
			Self._New(T.A, T.B, T.C, New Other(T.D))
		Else
			_New()
		Endif
	End
	
	Private
	
	' Monkey doesn't let you call another constructor from a constructor.
	' Because of this limitation, we have '_New' acting as a bridge between constructors.
	Method _New:Void(A:Int=0, B:Float=0.0, C:String="", D:Object=Null)
		Self.A = A
		Self.B = B
		Self.C = C
		
		Self.D = Null 'D
		
		Return
	End
	
	Public

	' Methods:
	Method QueueCompare:Bool(Info:Object)
		' Convert the object to our class.
		Local Data:Test = Test(Info)

		' If the 'Data' variable is null (It wasn't created with 'Test'), don't bother checking.
		If (Data = Null) Then Return False
		
		' Local variables:
		
		' The response given at the end of the function.
		Local Response:Bool = False
	
		' Compare the values of each variable.
		If (Data.A = A And Data.B = B And Data.C = C) Then
			If (D <> Null And Data.D <> Null) Then
				If (Data.D = D) Then
					Response = True
				Else
					If (Other(Data.D) <> Null And Other(D) <> Null) Then
						Response = Other(Data.D).QueueCompare(D)
					Endif
				Endif
			Elseif (D = Null And Data.D <> Null Or D <> Null And Data.D = Null)
				Response = False
			Else
				Response = True
			Endif
		Endif
	
		' If the objects weren't effectively the same, return false.
		Return Response
	End

	' Fields:
	Field A:Int
	Field B:Float
	Field C:String
	Field D:Object
End

' Basically just a container for a boolean:
Class Other Implements ComparableInQueue
	' Constructor(s):
	Method New(Value:Bool)
		Self.Boolean = Value
	End
	
	Method New(_O:Object=Null)
		Local O:= Other(_O)
		
		If (O <> Null) Then
			Self.Boolean = O.Boolean
		Else
			Self.Boolean = False
		Endif
	End

	' Methods:
	Method QueueCompare:Bool(D:Object)
		If (Other(D) And Other(D).Boolean = Self.Boolean) Then
			Return True
		Endif
	
		Return False
	End

	' Fields:
	Field Boolean:Bool
End