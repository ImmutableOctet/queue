Strict

Public

' Imports:
Import queue
Import example

' Functions:
Function Main:Int()
	Local Ints:= New Int[4]
	Local IntQueue:= New Queue<Int>()
	
	Ints[0] = 12345
	Ints[1] = 67890
	Ints[2] = 1011121314
	Ints[3] = 1617181920
	
	IntQueue = Queue<Int>.From(Ints)
	Print(" ")
	Print("Int-Array:")
	Print(" ")
	PrintCollection<Int[]>.Out(Ints, "I-Array")
	
	Print(" ")
	Print("Int-Queue:")
	Print(" ")
	PrintCollection<Queue<Int>>.Out(IntQueue, "I-Queue")

	Return 0
End