Strict

Public

' Imports:
Import queue

' Classes/Other:
Class PrintCollection<T>
	Function Out:Void(Lines:T, Message:String="")		
		If (Message <> "") Then Message += ": "
	
		For Local Line:String = Eachin Lines
			If (Message <> "") Then
				Print(Message + Line)
			Else
				Print(Line)
			Endif
		Next
		
		Return
	End
End

' Functions:
Function PrintStack:Void(Lines:Stack<String>, Message:String="")
	PrintCollection<Stack<String>>.Out(Lines, Message)
	
	Return
End

Function PrintQueue:Void(Lines:Queue<String>, Message:String="")
	PrintCollection<Queue<String>>.Out(Lines, Message)
	
	Return
End

Function PrintList:Void(Lines:List<String>, Message:String="")
	PrintCollection<List<String>>.Out(Lines, Message)

	Return
End

Function PrintArray:Void(Lines:String[], Message:String="")
	PrintCollection<String[]>.Out(Lines, Message)
	
	Return
End