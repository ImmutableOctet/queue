#Rem
DISCLAIMER:

Copyright (C) 2013 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.
#End

Strict

Public

' Preprocessor related:
#QUEUE_MONKEY_LEGACY = False
#QUEUE_ERROR_ON_EMPTY = True

' Imports:
Import compare
Import macros

' Classes:
Class Queue<T>
	' Constant variables:
	Const Default_QueueSize:Int = 16
	Const Default_IgnoreInfo:Bool = False
	Const Default_ReuseIndexes:Bool = True

	' Global variable(s):
	
	#Rem
		This variable is defined per-template; i.e. 'Queue<Int>' would have
		a global variable called 'NIL' of the type 'Int', which defaults to zero.
		This effect is same across all data types (Object types are 'Null', arrays are 'T[]'(Empty), etc).
	#End
	
	Global NIL:T

	' Functions (Public):
	Function CompareQueues:Bool(First:Queue<T>, Second:Queue<T>, CompareDetails:Bool=True)
		' If both queues point to the same object, return true.
		If (First = Second) Then Return True
		
		' Local variables:
		Local Response:Bool = True
		Local FirstData:T
		Local SecondData:T
		
		' This loop's implicit response is the value of the 'Response' variable (It doesn't do anything).
		For Local Index:Int = Max(First.OUT, Second.OUT) Until Min(First.IN, Second.IN)
			' The information we need to compare:
			FirstData = First.Data[Index]
			SecondData = Second.Data[Index]
			
			' If the variables aren't equal, look into this further.
			If (FirstData <> SecondData) Then
			
				' Check for the go-ahead to compare in detail.
				If (CompareDetails) Then
					' Check if the first object is comparable or not.
					If (ComparableInQueue(FirstData)) Then
						' The object has been detected as comparable,
						' now all we have to do is compare it to the second object.
						Response = ComparableInQueue(FirstData).QueueCompare(SecondData)
					Else
						' The first object/variable was not comparable.
						Response = False
					Endif
				Else
					' We didn't get the go-ahead to compare in detail.
					Response = False
				Endif
			Endif
			
			' In the event there wasn't a match, don't bother with the rest of the loop.
			If (Not Response) Then Exit
		Next
		
		' Now that we have the response, we need to return it.
		Return Response
	End
	
	' Conversion related functions (Some are actually constructors):
	Function FromStack:Queue<T>(S:Stack<T>)
		' Local variable(s):
		Local Q:= New Queue<T>()
		
		' Iterate through a backwards version of the stack,
		' and add its entries to this queue.
		For Local Data:T = Eachin S.Backwards()
			Q.Put(Data)
		Next
	
		Return Q
	End
	
	Function FromList:Queue<T>(L:List<T>)
		' Local variable(s):
		Local Q:= New Queue<T>()
		
		' Iterate through the list,
		' and its entries to this queue.
		For Local Data:T = Eachin L
			Q.Put(Data)
		Next
	
		Return Q
	End
	
	Function FromArray:Queue<T>(A:T[])
		' There's already a constructor for this,
		' we don't need to write anything here.
		Return New Queue<T>(A)
	End
	
	Function FromQueue:Queue<T>(Q:Queue<T>)
		Return New Queue<T>(Q)
	End
	
	#If QUEUE_MONKEY_LEGACY = 0
		Function FromQueue:Queue<T>(DQ:Deque<T>)
			Return New Queue<T>(DQ)
		End

		Function FromDeque:Queue<T>(DQ:Deque<T>)
			Return FromQueue(DQ)
		End
	#End
	
	' Very basic wrappers for the conversion commands:
	Function From:Queue<T>(S:Stack<T>)
		Return FromStack(S)
	End
	
	Function From:Queue<T>(L:List<T>)
		Return FromList(L)
	End
	
	Function From:Queue<T>(A:T[])
		Return FromArray(A)
	End
	
	Function From:Queue<T>(Q:Queue<T>)
		Return FromQueue(Q)
	End
	
	#If QUEUE_MONKEY_LEGACY = 0
		Function From:Queue<T>(DQ:Deque<T>)
			Return FromQueue(DQ)
		End
	#End

	' Constructor(s):
	#If QUEUE_MONKEY_LEGACY = 0
		Method New(DQ:Deque<T>, IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
			' Local variable(s):
			
			' The information passed in from the 'deque'.
			Local DQ_Data:T[] = DQ.ToArray()
			
			' Reverse the array so it looks like it was made in an 'FIFO' environment.
			ReverseArrayByRef(DQ_Data)
		
			' Set the initial size to the length of the new array from the 'deque'.
			Self.InitSize = DQ_Data.Length()
			
			' Assign the '_Data' variable to the new array from the 'deque'.
			Self.Data = DQ_Data
			
			' Set the ignore-mode to the information specified.
			Self.IgnoreInfo = IgnoreInfo
			
			' Set the index handling mode.
			Self.ReuseIndexes = ReuseIndexes
		End
	#End
	
	Method New(IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		' Set the initial size to the default.
		Self.InitSize = Default_QueueSize
		
		' Create an array sized accordingly.
		Self._Data = New T[InitSize]
		
		' Set the ignore-mode to the information specified.
		Self.IgnoreInfo = IgnoreInfo
		
		' Set the index handling mode.
		Self.ReuseIndexes = ReuseIndexes
	End

	Method New(Size:Int, IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		' Set the initial size to the size specified.
		Self.InitSize = Size
		
		' Create an array sized accordingly.
		Self._Data = New T[InitSize]
		
		' Set the ignore-mode to the information specified.
		Self.IgnoreInfo = IgnoreInfo
		
		' Set the index handling mode.
		Self.ReuseIndexes = ReuseIndexes
	End
	
	Method New(Info:T[], IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		' An array has been specified, so we don't need to create one.
		Data = Info
		
		' Set the initial size to the length of the information specified.
		Self.InitSize = Info.Length()
		
		' Set the ignore-mode to the information specified.
		Self.IgnoreInfo = IgnoreInfo
		
		' Set the index handling mode.
		Self.ReuseIndexes = ReuseIndexes
	End
	
	Method New(Q:Queue<T>)
		' There's already an array specified in the queue,
		' all we have to do is clone it.
		Self._Data = Q.Data.Resize(Q.Data.Length())
		
		' Set the ignore-mode to the information specified.
		Self.IgnoreInfo = Q.IgnoreInfo
		
		' Set the initial size to the initial size of the 'old' queue.
		Self.InitSize = Q.InitSize
		
		' Set the index handling mode to the 'old' queue's mode.
		Self.ReuseIndexes = Q.ReuseIndexes
		
		' The 'In' and 'Out' variables need to be the same as the other queue:
		Self.In = Q.IN
		Self.Out = Q.OUT
	End
	
	' Methods:
	Method Clear:Void(Flush:Bool=False, Remake:Bool=False)
		If (Remake) Then
			Self._Data = New T[InitSize]
		Else
			If (Flush) Then
				For Local Index:Int = 0 Until Data.Length()
					Data[Index] = NIL
				Next
			Endif
		Endif
	
		In = 0
		Out = 0
	
		Return
	End
	
	#Rem
	Method Init:Bool()
		' If we're ignoring old information, let the method work:
		If (IgnoreInfo) Then
			In = 0
			Out = 0
			
			Return True
		Endif
		
		Return False
	End
	#End
		
	Method Clone:Queue<T>()
		' Just as the 'FromQueue' function says,
		' we have a constructor for this already.
		Return New Queue<T>(Self)
	End
		
	Method Reverse:Queue<T>()
		' This method is a lot less efficient, but much easier.
		' Return Queue<T>.FromStack(ToStack())
	
		' Local variable(s):
		Local Q:= New Queue<T>(Size())
		
		' Min & Max cache:
		Local High:Int = Max(In, Out)
		Local Low:Int = Min(In, Out)
	
		' Simply iterate through the current queue,
		' and add the information to the queue in reverse.
		For Local Index:Int = Low Until High
			Q.Put(Data[(High-1)-(Index-Low)])
		Next
	
		' Now that we're done, we need to return the reversed queue.
		Return Q
	End
	
	Method ToArray:T[]()
		' Local variable(s):
		Local A:= New T[Size()]
		
		' Min & Max cache:
		Local High:Int = Max(In, Out)
		Local Low:Int = Min(In, Out)
		
		' Simply iterate through the queue,
		' and add the information to an array.
		For Local Index:Int = Low Until High
			A[Index-Low] = Data[Index]
		Next
		
		' Now that we've built it, we need to return the array.
		Return A
	End
	
	Method ToStack:Stack<T>()
		' Local variable(s):
		Local S:= New Stack<T>()
		
		' Min & Max cache:
		Local High:Int = Max(In, Out)
		Local Low:Int = Min(In, Out)
	
		' As usual, all we need to do is iterate through the queue's data variable.
		For Local Index:Int = Low Until High
			S.Push(Data[Index])
		Next
		
		' Now that we're finished, all we have to do is return the stack.
		Return S
	End
	
	Method ToList:List<T>()
		' Local variable(s):
		Local L:= New List<T>()
		
		' Min & Max cache:
		Local High:Int = Max(In, Out)
		Local Low:Int = Min(In, Out)
		
		' Once again, pretty self-explanatory.
		For Local Index:Int = Low Until High
			L.AddLast(Data[Index])
		Next
		
		' The list as been compiled, now we need to return it.
		Return L
	End
	
	' This command simply clones the queue.
	Method ToQueue:Queue<T>(); Return Clone(); End
	
	#If QUEUE_MONKEY_LEGACY = 0
		Method ToDeque:Deque<T>()
			Return New Deque<T>(ToArray())
		End
	#End
	
	' Just a quick wrapper for 'CompareQueues'.
	Method Compare:Bool(Q:Queue<T>, CompareDetails:Bool=True)
		Return CompareQueues(Self, Q, CompareDetails)
	End
	
	Method Put:Void(Value:T)
		' Local variable(s):
		
		' Cache the length of the array,
		' so we don't need to keep calling the function.
		Local DataLength:Int = Data.Length()
		Local High:Int = Max(In, Out)
		Local Low:Int = Min(In, Out)
	
		' First thing's first; we need to optimize where we can:
		If (ReuseIndexes And Out >= InitSize And (Out Mod InitSize) = 0) Then
			For Local Index:Int = Low Until High
				Data[Index-Low] = Data[Max(Index, 0)]
				Data[Max(Index, 0)] = NIL
			Next
			
			In -= Low
			Out -= Low
		Else
			' Check if the internal buffer is full:
			If (In >= DataLength) Then
				' The buffer is full, and we need to resize it.
				Self._Data = _Data.Resize(DataLength * 2) 'Int(DataLength * 1.5)
			Endif
		Endif
		
		' Assign an index in the array to the value specified.
		Data[In] = Value
		
		' Increment the 'In' variable by one.
		In = (In + 1) 'Mod Data.Length()
	End
	
	' Nothing to see here, just some wrappers for 'Put':
	Method Push:Void(Value:T); Put(Value); Return; End
	Method Add:Void(Value:T); Put(Value); Return; End
	Method Set:Void(Value:T); Put(Value); Return; End
	
	' This command is just short-hand for 'Not IsEmpty()'.
	Method IsFull:Bool(); Return Not IsEmpty(); End
	
	' This command is just short-hand for checking if the 'count' is zero.
	Method IsEmpty:Bool()
		Return (Count() = 0)
	End

	Method Get:T()
		' Local variable(s):
		Local Value:T
	
		' If the internal data-buffer is empty, don't bother doing anything.
		If (IsEmpty()) Then
			#If CONFIG = "debug" And QUEUE_ERROR_ON_EMPTY = 1
				Error("Error: Attempted to access data from an empty queue.")
			#End
		Else
			' However, if it's not empty, get the information:
			Value = Data[Out]
			If (Not IgnoreInfo) Then Data[Out] = NIL

			Out = (Out+1) 'Mod Data.Length()'+1
		Endif
		
		Return Value
	End
	
	' Just some quick wrappers for 'Get':
	Method Remove:T(); Return Get(); End
	Method Pop:T(); Return Get(); End

	' Monkey automatically detects this method,
	' and then proceeds to use the returned enumerator.
	Method ObjectEnumerator:Enumerator<T>()
		Return New Enumerator<T>(Self)
	End
	
	' Properties (Public):
	
	' The 'Size' method returns the actual number of entries.
	Method Size:Int() Property
		Return (Max(In, Out) - Min(In, Out))
	End
	
	' The usual wrappers for the sake of preference:
	Method Count:Int() Property
		Return Size()
	End
	
	Method Length:Int() Property
		Return Size()
	End
	
	' Whenever you need to access the 'In' and 'Out', fields from outside this class, use these:
	Method IN:Int() Property
		Return Self.In
	End
	
	Method OUT:Int() Property
		Return Self.Out
	End
	
	' Simple property-wrappers for '_Data':
	Method Data:Void(Info:T[]) Property
		Self._Data = Info.Resize(Info.Length())
		
		Self.In = Info.Length()
		Self.Out = 0
		Self.InitSize = Info.Length()
		
		Return
	End

	' This just returns the array at the moment.
	Method Data:T[]() Property
		Return Self._Data
	End
	
	' This returns the initial size of the queue.
	Method InitSize:Int() Property
		Return Self._InitSize
	End

	' Private methods:
	Private
	
	' Nothing so far.
	
	Public
	
	' Private properties:
	Private
	
	' This property acts as the assignment method for 'InitSize'.
	Method InitSize:Void(Info:Int) Property
		Self._InitSize = Info
	
		Return
	End
	
	Public

	' Fields (Public):
	
	#Rem
		If this variable is true, data already returned to the user will remain in the queue.
		However, if this variable is false, data will be set to 'NIL' immediately.
		Later down the road I may reuse old indexes that have already been set to 'NIL'.
		If that ends up happening, this variable and 'InitSize' will be quite valuable.
	#End
	Field IgnoreInfo:Bool
	
	' The setting for reuse of indexes.
	Field ReuseIndexes:Bool

	' Fields (Private):
	Private
	
	' The actual information held in the queue.
	Field _Data:T[]
	
	' The number of times 'Put' has been called.
	Field In:Int
	
	' The number of times 'Get' has been called.
	Field Out:Int
	
	' See the description for 'InitSize'.
	Field _InitSize:Int
	
	Public
	
	' INSERT ANY OTHER CODE HERE.
	
	Private
	
	' Utility functions (Private):
	Function ReverseArray:T[](DataIn:T[])
		' Local variable(s):
		
		' Make a quick clone of 'DataIn', and use it as our output.
		Local DataOut:= DataIn.Resize(DataIn.Length())
		
		' Now that we've got that settled, call the reference/pointer-based version of this command.
		ReverseArrayByRef(DataOut)
		
		' Return the output array. (Which is now in reverse)
		Return DataOut
	End
	
	Function ReverseArrayByRef:Void(DataIn:T[])
		' Local variables:
			
		' Cache the data length for the sake of optimization.
		Local DataLength:Int = DataIn.Length()
		
		' Our variable to hold information while we're switching indexes.
		Local Temp:T
	
		' Simply iterate through the array, and reverse the elements.
		For Local Index:Int = 0 Until DataLength
			' C ('Temp') = A ('DataIn' of the current index)
			Temp = DataIn[Index]
			
			' A ('DataIn' of the current index) = B ('DataIn' of the opposite index)
			DataIn[Index] = DataIn[(DataLength-1)-Index]
			
			' B ('DataIn' of the opposite index) = C (Which is a 'copy' of A)
			DataIn[(DataLength-1)-Index] = Temp
		Next
	
		' No need to return anything.
		Return
	End
	
	Public
End

' The queue class's enumerator. (See 'Queue<T>.ObjectEnumerator' for details)
Class Enumerator<T>
	' Constructor(s):
	Method New(Q:Queue<T>)
		' Check if the queue is empty or null:
		#If CONFIG = "debug" And QUEUE_ERROR_ON_EMPTY = 1
			If (Q = Null) Then
				Error("Error: Enumeration attempted on a null queue.")
			Else
				If (Q.IsEmpty()) Then
					Error("Error: Enumeration attempted on an empty queue.")
				Endif
			Endif
		#End
	
		' Assign 'Self.Q' to the queue specified.
		Self.Q = Q
	End

	' Methods:
	Method HasNext:Bool()
		If (Q = Null) Then Return False
	
		' Make sure the queue isn't empty.
		Return Not Q.IsEmpty()
	End

	Method NextObject:T()
		If (Q = Null) Then Return Queue<T>.NIL
	
		' Now that we've determined there is another entry, get it and return it.
		Return Q.Get()
	End

	' Private entries:
	Private
	
	' Fields:
	
	' The queue we're enumerating.
	Field Q:Queue<T>
	
	' The current 'index'.
	Field Index:Int
	
	' Public entries:
	' Nothing so far.
End