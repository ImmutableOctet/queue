Strict

Public

' Imports:
Import queue

' Classes:
' Nothing so far.

' Interfaces:
Interface ComparableInQueue
	Method QueueCompare:Bool(Data:Object)
End