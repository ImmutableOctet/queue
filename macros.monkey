'#Rem

Strict

Public

' Imports:
Import queue

' Classes:
Class IntQueue Extends Queue<Int>
	' Functions:
	Function Create:IntQueue()
		Return New IntQueue()
	End
	
	' Constructor(s):
	#If QUEUE_MONKEY_LEGACY = 0
		Method New(DQ:Deque<Int>, IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
			Super.New(DQ, IgnoreInfo, ReuseIndexes)
		End
	#End
	
	Method New(IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		Super.New(IgnoreInfo, ReuseIndexes)
	End
	
	Method New(Size:Int, IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		Super.New(Size, IgnoreInfo, ReuseIndexes)
	End
	
	Method New(Info:Int[], IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		Super.New(Info, IgnoreInfo, ReuseIndexes)
	End
	
	Method New(Q:Queue<Int>)
		Super.New(Q)
	End
End

Class FloatQueue Extends Queue<Float>
	' Functions:
	Function Create:FloatQueue()
		Return New FloatQueue()
	End

	' Constructor(s):
	#If QUEUE_MONKEY_LEGACY = 0
		Method New(DQ:Deque<Float>, IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
			Super.New(DQ, IgnoreInfo, ReuseIndexes)
		End
	#End
	
	Method New(IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		Super.New(IgnoreInfo, ReuseIndexes)
	End
	
	Method New(Size:Float, IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		Super.New(Size, IgnoreInfo, ReuseIndexes)
	End
	
	Method New(Info:Float[], IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		Super.New(Info, IgnoreInfo, ReuseIndexes)
	End
	
	Method New(Q:Queue<Float>)
		Super.New(Q)
	End
End

Class StringQueue Extends Queue<String>
	' Functions:
	Function Create:StringQueue()
		Return New StringQueue()
	End
	
	' Constructor(s):
	#If QUEUE_MONKEY_LEGACY = 0
		Method New(DQ:Deque<String>, IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
			Super.New(DQ, IgnoreInfo, ReuseIndexes)
		End
	#End
	
	Method New(IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		Super.New(IgnoreInfo, ReuseIndexes)
	End
	
	Method New(Size:String, IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		Super.New(Size, IgnoreInfo, ReuseIndexes)
	End
	
	Method New(Info:String[], IgnoreInfo:Bool=Default_IgnoreInfo, ReuseIndexes:Bool=Default_ReuseIndexes)
		Super.New(Info, IgnoreInfo, ReuseIndexes)
	End
	
	Method New(Q:Queue<String>)
		Super.New(Q)
	End
End

' Functions:
Function CreateQueue:IntQueue()
	Return CreateIntQueue()
End

Function CreateIntQueue:IntQueue()
	Return IntQueue.Create()
End

Function CreateFloatQueue:FloatQueue()
	Return FloatQueue.Create()
End

Function CreateStringQueue:StringQueue()
	Return StringQueue.Create()
End

'#End